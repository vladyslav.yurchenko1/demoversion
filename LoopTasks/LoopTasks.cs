﻿using System;

namespace LoopTasks
{
    public static class LoopTasks
    {
        public static int SumOfOddDigits(int n)
        {
            int result = 0;
            int lastNumber;
            while (n > 0)
            {
                lastNumber = n % 10;
                if (lastNumber % 2 != 0)
                {
                    result += lastNumber;
                }
                n /= 10;
            }
            return result;
        }

        public static int NumberOfUnitsInBinaryRecord(int n)
        {
            int count = 0;


            while (n > 0)
            {
                if (n % 2 == 1)
                {
                    count++;
                }
                n = n / 2;
            }
            return count;
        }

        public static int SumOfFirstNFibonacciNumbers(int n)
        {
            int currentFib = 0;
            int tempNumber;
            int sumOfFib = 0;
            int nextFib = 1;
            for (int i = 0; i < n; i++)
            {
                tempNumber = currentFib;
                currentFib = nextFib;
                nextFib = tempNumber + nextFib;
                sumOfFib += tempNumber;
            }
            return sumOfFib;
        }
    }
}